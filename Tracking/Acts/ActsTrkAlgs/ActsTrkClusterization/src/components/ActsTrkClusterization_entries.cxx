/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "src/PixelClusterizationAlg.h"

DECLARE_COMPONENT(ActsTrk::PixelClusterizationAlg)
