/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "src/SeedingAlg.h"
#include "src/SeedingFromAthenaAlg.h"

DECLARE_COMPONENT( ActsTrk::SeedingAlg )
DECLARE_COMPONENT( ActsTrk::SeedingFromAthenaAlg )
